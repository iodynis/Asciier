﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Iodynis.Libraries.Templating
{
    /// <summary>
    /// Provides ASCII-art functionalty.
    /// </summary>
    public class Asciier
    {
        /// <summary>
        /// Height of a symbol of the alphabet.
        /// </summary>
        public int Height { get; }
        private int Padding { get; set; } = 1;

        private readonly Dictionary<char, List<string>> Dictionary = new Dictionary<char, List<string>>();
        /// <summary>
        /// Provides ASCII-art functionalty.
        /// </summary>
        /// <param name="pathOrContent">Path to the file or its contents that contain the alphabet definition.</param>
        public Asciier(string pathOrContent)
        {
            string content = null;

            // If path
            if (!pathOrContent.Contains('\r') && !pathOrContent.Contains('\n'))
            {
                string path = Path.GetFullPath(pathOrContent);
                if (!File.Exists(path))
                {
                    throw new FileNotFoundException(path);
                }

                using (FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, 4096, FileOptions.SequentialScan))
                using (StreamReader streamReader = new StreamReader(fileStream, Encoding.UTF8))
                {
                    content = streamReader.ReadToEnd();
                }
            }
            // If content
            else
            {
                content = pathOrContent;
            }

            // Split into lines
            string[] lines = content.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

            // If empty
            if (lines.Length == 0)
            {
                return;
            }

            // First line is a sequence of all chracters presented
            char[] characters = lines[0].ToCharArray();

            // Calculate the height the elements
            Height = (lines.Length - 1) / characters.Length;

            // Populate the dictionary
            for (int characterIndex = 0; characterIndex < characters.Length; characterIndex++)
            {
                char character = characters[characterIndex];
                List<string> characterLines = new List<string>();
                for (int lineIndex = 0; lineIndex < Height; lineIndex++)
                {
                    characterLines.Add(lines[1 + characterIndex * Height + lineIndex]);
                }
                if (Dictionary.ContainsKey(character))
                {
                    throw new Exception($"Character {character} is defined more than once.");
                }
                Dictionary.Add(character, characterLines);
            }
        }
        /// <summary>
        /// Draw the text with ASCII-art alphabet.
        /// </summary>
        /// <param name="text">Text to draw.</param>
        /// <returns>Text representation using the ASCII-art alphabet.</returns>
        public string Asciify(string text)
        {
            return Asciify(text, Padding);
        }
        /// <summary>
        /// Draw the text with ASCII-art alphabet.
        /// </summary>
        /// <param name="text">Text to draw.</param>
        /// <param name="padding">Custom padding between symbols.</param>
        /// <returns>Text representation using the ASCII-art alphabet.</returns>
        public string Asciify(string text, int padding)
        {
            string paddingString = "";
            for (int i = 0; i < padding; i++)
            {
                paddingString += " ";
            }

            StringBuilder stringBuilder = new StringBuilder();
            foreach (string line in text.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries))
            {
                char[] characters = line.ToCharArray();
                for (int lineIndex = 0; lineIndex < Height; lineIndex++)
                {
                    for (int characterIndex = 0; characterIndex < characters.Length; characterIndex++)
                    {
                        Dictionary.TryGetValue(characters[characterIndex], out List<string> lines);
                        // Skip unknown characters
                        if (lines == null)
                        {
                            continue;
                        }
                        if (characterIndex != 0)
                        {
                            stringBuilder.Append(paddingString);
                        }
                        stringBuilder.Append(lines[lineIndex]);
                    }
                    stringBuilder.Append(Environment.NewLine);
                }
                stringBuilder.Append(Environment.NewLine);
            }
            return stringBuilder.ToString();
        }
    }
}
